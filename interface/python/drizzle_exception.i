/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


%{
  #define D_exception(excp, code, msg) { \
      drizzle_throw_exception(excp,code,msg); \
      SWIG_fail; }

  //TODO: Need to support this form of exception
  #define D_exception_err(excp, err) { \
      drizzle_throw_exception(excp, err.code, err.message);\
      SWIG_fail; }
      
  #define GET_pyexcept(EXCPT) \
      PyObject_GetAttrString(PyObject_Drizzle_Errors, EXCPT)


  PyObject *PyObject_Drizzle_Errors= NULL;

  void drizzle_throw_exception(drizzle_exception excp_mod,
                               int code, const char * msg) {

    PyObject * exception;

    switch (excp_mod) {
    case DrizzleWarning:
      exception = GET_pyexcept("Warning");
      break;
    case DrizzleError:
      exception = GET_pyexcept("Error");
      break;
    case InterfaceError:
      switch (code) {
      case DRIZZLE_RETURN_GETADDRINFO:
        exception = GET_pyexcept("AddressError");
        break;
      case DRIZZLE_RETURN_AUTH_FAILED:
        exception = GET_pyexcept("AuthFailedError");
        break;
      case DRIZZLE_RETURN_LOST_CONNECTION:
        exception = GET_pyexcept("LostConnectionError");
        break;
      case DRIZZLE_RETURN_COULD_NOT_CONNECT:
        exception = GET_pyexcept("CouldNotConnectError");
        break;
      default:
        exception = GET_pyexcept("LibDrizzleError");
        break;
      }
      break;
    case DatabaseError:
      exception = GET_pyexcept("DatabaseError");
      break;
    case DataError:
      exception = GET_pyexcept("DataError");
      break;
    case OperationalError:
      exception = GET_pyexcept("OperationalError");
      break;    
    case IntegrityError:
      exception = GET_pyexcept("IntegrityError");
      break;
    case InternalError:
      exception = GET_pyexcept("InternalError");
      break;
    case ProgrammingError:
      exception = GET_pyexcept("ProgrammingError");
      break;
    case NotSupportedError:
      exception = GET_pyexcept("NotSupportedError");
      break;
    default:
      exception = PyExc_RuntimeError;
    }
    if (exception == NULL) {
      exception = PyExc_RuntimeError;
    }
    PyErr_SetObject(exception, PyTuple_Pack(2, PyInt_FromLong(code), SWIG_FromCharPtr(msg)));
    
    /* I don't believe this is necessary...
      Py_INCREF(exception);
    */
  }
  
  
  bool check_drizzle_return(drizzle_return_t ret, drizzle_con_st *con) {
    if (ret == DRIZZLE_RETURN_OK) {
      return true;
    } else if (ret == DRIZZLE_RETURN_ERROR_CODE) {
      drizzle_throw_exception(DatabaseError, drizzle_con_error_code(con), drizzle_con_error(con));
      return false;
    } else {
      drizzle_throw_exception(InterfaceError, ret, drizzle_con_error(con));
      return false;
    }
  }
%}

%init %{

  PyObject_Drizzle_Errors = PyImport_ImportModule("drizzle.errors");
  
%}
