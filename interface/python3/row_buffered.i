/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

%typemap(argout) (row_buffered *buffer)
{
  if ($1->row == NULL)
  {
    return Py_None;
  }

  PyObject *row_tuple= PyTuple_New((Py_ssize_t)($1->field_count+1));

  uint16_t row;
  size_t *field_sizes= drizzle_row_field_sizes($1->result);

  PyObject *field_buffer= NULL;

  for (row= 0; row < $1->field_count; row++)
  {
    field_buffer= PyBuffer_FromMemory($1->row[row], field_sizes[row]);

    PyTuple_SetItem(row_tuple, row, field_buffer);
  }
  /* Save a pointer to the row pointer in the last element of the tuple
   * so that we can use it in row_free
   */
  field_buffer= PyBuffer_FromMemory($1->row, $1->field_count);
  PyTuple_SetItem(row_tuple, row, field_buffer);

  $result= row_tuple;
}
