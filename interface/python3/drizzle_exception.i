/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


%{

  /* Children of Exception */
  PyObject *PyExc_Warning= NULL;
  PyObject *PyExc_Error= NULL;

  /* Children of Error */
  PyObject *PyExc_InterfaceError= NULL;
  PyObject *PyExc_DatabaseError= NULL;

  /* Children of DatabaseError */
  PyObject *PyExc_DataError= NULL;
  PyObject *PyExc_OperationalError= NULL;
  PyObject *PyExc_IntegrityError= NULL;
  PyObject *PyExc_InternalError= NULL;
  PyObject *PyExc_ProgrammingError= NULL;
  PyObject *PyExc_NotSupportedError= NULL;

  void drizzle_throw_exception(drizzle_exception excp_mod,
                               int code, const char * msg) {

    PyObject * exception;

    switch (excp_mod) {
    case DrizzleWarning:
      exception = PyExc_Warning;
      break;
    case DrizzleError:
      exception = PyExc_Error;
      break;
    case InterfaceError:
      exception = PyExc_InterfaceError;
      break;
    case DatabaseError:
      exception = PyExc_DatabaseError;
      break;
    case DataError:
    case OperationalError:
    case IntegrityError:
    case InternalError:
    case ProgrammingError:
    case NotSupportedError:
    default:
      exception = PyExc_RuntimeError;
    }
    if (exception == NULL) {
      exception = PyExc_RuntimeError;
    }
    PyErr_SetString(exception,msg);
    Py_INCREF(exception);
  }

#define D_exception(excp, code, msg) { \
    drizzle_throw_exception(excp, code, msg); \
    SWIG_fail; }

//TODO: Need to support this form of exception
#define D_exception_err(excp, err) { \
    drizzle_throw_exception(excp, err.code, err.message);\
    SWIG_fail; }

#define NEW_pyexcept(EXCPT,EPARENT) { \
    PyExc_ ## EXCPT = \
      PyErr_NewException((char *)"drizzle.db." #EXCPT ,\
                         PyExc_ ## EPARENT,NULL);\
    PyDict_SetItemString(d, #EXCPT ,PyExc_ ## EXCPT);\
    Py_INCREF(PyExc_ ## EXCPT); }

#define NEW_BASE_pyexcept(EXCPT) { \
    PyExc_ ## EXCPT = \
      PyErr_NewException((char *)"drizzle.db." #EXCPT ,\
                         PyExc_Exception, NULL);\
    PyDict_SetItemString(d, #EXCPT ,PyExc_ ## EXCPT);\
    Py_INCREF(PyExc_ ## EXCPT); }

  %}

%init %{


  NEW_BASE_pyexcept(Warning);
  NEW_BASE_pyexcept(Error);
  NEW_pyexcept(InterfaceError, Error);
  NEW_pyexcept(DatabaseError, Error);
  NEW_pyexcept(DataError, DatabaseError);
  NEW_pyexcept(OperationalError, DatabaseError);
  NEW_pyexcept(IntegrityError, DatabaseError);
  NEW_pyexcept(InternalError, DatabaseError);
  NEW_pyexcept(ProgrammingError, DatabaseError);
  NEW_pyexcept(NotSupportedError, DatabaseError);


  %}

%pythoncode %{

Warning=_libdrizzle.Warning
Error=_libdrizzle.Error
InterfaceError=_libdrizzle.InterfaceError
DatabaseError=_libdrizzle.DatabaseError
DataError=_libdrizzle.DataError
OperationalError=_libdrizzle.OperationalError
IntegrityError=_libdrizzle.IntegrityError
InternalError=_libdrizzle.InternalError
ProgrammingError=_libdrizzle.ProgrammingError
NotSupportedError=_libdrizzle.NotSupportedError

%}

