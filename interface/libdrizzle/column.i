/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


typedef struct drizzle_column_st {} Column;

%extend Column
{

  ~Column()
  {
    drizzle_column_free($self);
  }

  Result *result()
  {
    return drizzle_column_drizzle_result($self);
  }

  unicode catalog()
  {
    return drizzle_column_catalog($self);
  }

  unicode db()
  {
    return drizzle_column_catalog($self);
  }

  unicode table()
  {
    return drizzle_column_table($self);
  }
  unicode orig_table()
  {
    return drizzle_column_orig_table($self);
  }
  unicode name()
  {
    return drizzle_column_name(self);
  }
  unicode orig_name()
  {
    return drizzle_column_orig_name(self);
  }
  const uint16_t charset()
  {
    return drizzle_column_charset(self);
  }
  uint32_t size()
  {
    return drizzle_column_size(self);
  }
  size_t max_size()
  {
    return drizzle_column_max_size(self);
  }
  drizzle_column_type_t column_type()
  {
    return drizzle_column_type(self);
  }
  drizzle_column_flags_t flags()
  {
    return drizzle_column_flags(self);
  }
  uint8_t decimals()
  {
    return drizzle_column_decimals(self);
  }
  void default_value(char **ret_val, size_t *ret_size)
  {
    *ret_val= (char *)drizzle_column_default_value(self, ret_size);
  }


  /**
   * Set catalog name for a column.
   */
  void set_catalog(const char *catalog)
  {
    drizzle_column_set_catalog($self, catalog);
  }


  /**
   * Set database name for a column.
   */
  void set_db(const char *db)
  {
    drizzle_column_set_db($self, db);
  }


  /**
   * Set table name for a column.
   */
  void set_table(drizzle_column_st *column, const char *table)
  {
    drizzle_column_set_table($self, table);
  }


  /**
   * Set original table name for a column.
   */
  void set_orig_table(const char *orig_table)
  {
    drizzle_column_set_orig_table($self, orig_table);
  }


  /**
   * Set column name for a column.
   */
  void set_name(const char *name)
  {
    drizzle_column_set_name($self, name);
  }


  /**
   * Set original column name for a column.
   */
  void set_orig_name(const char *orig_name)
  {
    drizzle_column_set_orig_name($self, orig_name);
  }


  /**
   * Set charset for a column.
   */
  void set_charset(drizzle_charset_t charset)
  {
    drizzle_column_set_charset($self, charset);
  }


  /**
   * Set size of a column.
   */
  void set_size(uint32_t size)
  {
    drizzle_column_set_size($self, size);
  }


  /**
   * Set the type of a column.
   */
  void set_type(drizzle_column_type_t type)
  {
    drizzle_column_set_type($self, type);
  }


  /**
   * Set flags for a column.
   */
  void set_flags(drizzle_column_flags_t flags)
  {
    drizzle_column_set_flags($self, flags);
  }


  /**
   * Set the number of decimals for numeric columns.
   */
  void set_decimals(uint8_t decimals)
  {
    drizzle_column_set_decimals($self, decimals);
  }


  /**
   * Set default value for a column.
   */
  void set_default_value(const uint8_t *default_value, size_t size)
  {
    drizzle_column_set_default_value($self, default_value, size);
  }


}
