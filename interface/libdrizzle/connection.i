/* -*- mode: c; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

typedef struct drizzle_con_st {} Connection;

%extend Connection {

  /* Set options for a connection. */
  unicode host()
  {
    return drizzle_con_host($self);
  }

  in_port_t port()
  {
    return drizzle_con_port($self);
  }

  void set_tcp(char *host, in_port_t port)
  {
    drizzle_con_set_tcp($self, host, port);
  }

  unicode uds()
  {
    return drizzle_con_uds($self);
  }

  void set_uds(const char *uds)
  {
    drizzle_con_set_uds($self, uds);
  }

  unicode user()
  {
    return drizzle_con_user($self);
  }

  unicode password()
  {
    return drizzle_con_password($self);
  }

  void set_auth(const char *user, const char *password)
  {
    drizzle_con_set_auth($self, user, password);
  }

  unicode db()
  {
    return drizzle_con_db($self);
  }

  void set_db(char *db)
  {
    drizzle_con_set_db($self, db);
  }

  drizzle_con_options_t options()
  {
    return drizzle_con_options($self);
  }
  
  void set_options(drizzle_con_options_t options)
  {
    drizzle_con_set_options($self, options);
  }

  void add_options(drizzle_con_options_t options)
  {
    drizzle_con_add_options($self, options);
  }

  void remove_options(drizzle_con_options_t options)
  {
    drizzle_con_remove_options($self, options);
  }

  /* Connect to server. */
  drizzle_return_t connect()
  {
    return drizzle_con_connect($self);
  }

  /* Close a connection. */
  void close()
  {
    drizzle_con_close($self);
  }

  /* Get information on connection. */
  uint8_t protocol_version()
  {
    return drizzle_con_protocol_version($self);
  }

  unicode server_version()
  {
    return drizzle_con_server_version($self);
  }

  uint32_t server_version_number()
  {
    return drizzle_con_server_version_number($self);
  }

  uint32_t thread_id()
  {
    return drizzle_con_thread_id($self);
  }

  const uint8_t *scramble()
  {
    return drizzle_con_scramble($self);
  }

  drizzle_capabilities_t capabilities()
  {
    return drizzle_con_capabilities($self);
  }

  drizzle_charset_t charset()
  {
    return drizzle_con_charset($self);
  }

  drizzle_con_status_t status()
  {
    return drizzle_con_status($self);
  }

  uint32_t max_packet_size()
  {
    return drizzle_con_max_packet_size($self);
  }


  %exception {
    $action
    if (check_drizzle_return(drizzle_return_value, arg1) == false) {
      SWIG_fail;
    }
  }

  /**
   * Send query to server.
   */
  Result *query(const char *query, size_t size,
                drizzle_return_t *drizzle_return_value)
  {
    return drizzle_query($self, NULL, query, size, drizzle_return_value);
  }


  /* Send query incrementally. */
  Result *query_incremental(const char *query, size_t size,
                            size_t total,
                            drizzle_return_t *drizzle_return_value)
  {
    return drizzle_query_inc($self, NULL, query, size, total,
                             drizzle_return_value);
  }

  /* Send other requests to server. */
  Result *quit(drizzle_return_t *drizzle_return_value)
  {
    return drizzle_quit($self, NULL, drizzle_return_value);
  }

  Result *select_db(char *db, drizzle_return_t *drizzle_return_value)
  {
    return drizzle_select_db($self, NULL, db, drizzle_return_value);
  }

  Result *shutdown(uint32_t level, drizzle_return_t *drizzle_return_value)
  {
    return drizzle_shutdown($self, NULL, level, drizzle_return_value);
  }

  Result *ping(drizzle_return_t *drizzle_return_value)
  {
    return drizzle_con_ping($self, NULL, drizzle_return_value);
  }

  Result *result_read(drizzle_return_t *drizzle_return_value,
                      drizzle_result_st *to=NULL)
  {
    return drizzle_result_read($self, to, drizzle_return_value);
  }

  Result *write(drizzle_command_t command, const uint8_t *data, size_t size,
                size_t total, drizzle_return_t *drizzle_return_value)
  {
    return drizzle_con_command_write($self, NULL, command, data,
                                 size, total, drizzle_return_value);
  }

  %noexception;

  Result *result_create(drizzle_result_st *to=NULL)
  {
    return drizzle_result_create($self, to);
  }

  Result *result_clone(drizzle_result_st *clone_from)
  {
    return drizzle_result_clone($self, NULL, clone_from);
  }

  drizzle_return_t result_write(drizzle_result_st *to, bool flush)
  {
    return drizzle_result_write($self, to, flush);
  }

  /**
   * Set protocol version for a connection.
   */
  void set_protocol_version(uint8_t protocol_version)
  {
    drizzle_con_set_protocol_version($self, protocol_version);
  }

  /**
   * Set server version string for a connection.
   */
  void set_server_version(const char *server_version)
  {
    drizzle_con_set_server_version($self, server_version);
  }

  /**
   * Set thread ID for a connection.
   */
  void set_thread_id(uint32_t thread_id)
  {
    drizzle_con_set_thread_id($self, thread_id);
  }

  /**
   * Set scramble buffer for a connection.
   */
  void set_scramble(const uint8_t *scramble)
  {
    drizzle_con_set_scramble($self, scramble);
  }

  /**
   * Set capabilities for a connection.
   */
  void set_capabilities(drizzle_capabilities_t capabilities)
  {
    drizzle_con_set_capabilities($self, capabilities);
  }

  /**
   * Set charset for a connection.
   */
  void set_charset(drizzle_charset_t charset)
  {
    drizzle_con_set_charset($self, charset);
  }

  /**
   * Set status for a connection.
   */
  void set_status(drizzle_con_status_t status)
  {
    drizzle_con_set_status($self, status);
  }

  /**
   * Set max packet size for a connection.
   */
  void set_max_packet_size(uint32_t max_packet_size)
  {
    drizzle_con_set_max_packet_size($self, max_packet_size);
  }

  /**
   * Copy all handshake information from one connection into another.
   */
  void copy_handshake(drizzle_con_st *source)
  {
    drizzle_con_copy_handshake($self, source);
  }

}
