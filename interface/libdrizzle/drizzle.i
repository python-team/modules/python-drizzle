/* -*- mode: c; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 * vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


char *drizzle_version(void);

typedef struct drizzle_st {} Drizzle;

%extend Drizzle {

  /* Initialize a library instance structure. */
  Drizzle(Drizzle *d= NULL)
  {
    return drizzle_create(d);
  }

  /* Free a library instance structure. */
  ~Drizzle()
  {
    drizzle_free($self);
  }

  /* Clone a library instance structure. */
  Drizzle *copy()
  {
    return drizzle_clone(NULL, $self);
  }

  /* Return an error string for last library error encountered. */
  unicode error()
  {
    return drizzle_error($self);
  }

  /* Value of errno in the case of a DRIZZLE_RETURN_ERRNO return value. */
  int errno()
  {
    return drizzle_errno($self);
  }

  drizzle_options_t options()
  {
    return drizzle_options($self);
  }

  void set_options(drizzle_options_t options)
  {
    drizzle_set_options($self, options);
  }

  void add_options(drizzle_options_t options)
  {
    drizzle_add_options($self, options);
  }

  void remove_options(drizzle_options_t options)
  {
    drizzle_remove_options($self, options);
  }

  Connection *create_connection() {
    return drizzle_con_create($self, NULL);
  }

  /* Wait for I/O on connections. */
  drizzle_return_t con_wait()
  {
    return drizzle_con_wait($self);
  }

  /* Get next connection that is ready for I/O. */
  Connection *con_ready()
  {
    return drizzle_con_ready($self);
  }

  Connection *add_tcp(const char *host,
                      in_port_t port,
                      const char *user,
                      const char *password,
                      const char *db,
                      drizzle_con_options_t options)
  {
    return drizzle_con_add_tcp($self, NULL, host, port, user, password,
                               db, options);
  }

  Connection *add_uds(const char *uds,
                      const char *user,
                      const char *password,
                      const char *db,
                      drizzle_con_options_t options)
  {
    return drizzle_con_add_uds($self, NULL, uds, user, password,
                               db, options);
  }

  /* Initialize a query structure. */
  Query *create_query()
  {
    return drizzle_query_create($self, NULL);
  }

  /* Add a query to be run concurrently. */
  Query *add_query(Query *query,
                   Connection *con,
                   const char *query_string,
                   size_t len,
                   drizzle_query_options_t options,
                   void *context) {
    return drizzle_query_add($self, query, con, NULL, query_string, len,
                             options, context);
  }

  Query *run()
  {
    /* TODO: Need to interpret ret here */
    drizzle_return_t ret;
    return drizzle_query_run($self, &ret);
  }


  drizzle_return_t run_all_queries()
  {
    return drizzle_query_run_all($self);
  }


}
