/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


typedef struct drizzle_result_st {} Result;

%extend Result {

  ~Result() {
    drizzle_result_free($self);
  }

  Connection *connection()
  {
    return drizzle_result_drizzle_con($self);
  }

  /**
   * Get an array of all field sizes for buffered rows.
   */
  size_t *row_field_sizes()
  {
    return drizzle_row_field_sizes($self);
  }

  /**
   * Get next buffered row from a fully buffered result.
   */
  drizzle_row_t next_row(row_buffer *result_buffer)
  {
    result_buffer->row= drizzle_row_next($self);
    result_buffer->field_count= drizzle_result_column_count($self);
    result_buffer->result= $self;
    return (result_buffer->row);
  }

  /**
   * Get previous buffered row from a fully buffered result.
   */
  drizzle_row_t prev_row(row_buffer *result_buffer)
  {
    result_buffer->row= drizzle_row_prev($self);
    result_buffer->field_count= drizzle_result_column_count($self);
    result_buffer->result= $self;
    return (result_buffer->row);
  }

  /**
   * Get a field at the specified position from the given row.
   */
  const char *get_row_field(drizzle_row_t row, int pos)
  {
    if (pos > drizzle_result_column_count($self))
      return NULL;
    return row[pos];
  }

  /**
   * Seek to the given buffered row in a fully buffered result.
   */
  void row_seek(uint64_t row)
  {
    drizzle_row_seek($self, row);
  }

  /**
   * Get the given buffered row from a fully buffered result.
   */
  void row_index(row_buffer *result_buffer, uint64_t row)
  {
    result_buffer->row= drizzle_row_index($self, row);
    result_buffer->field_count= drizzle_result_column_count($self);
    result_buffer->result= $self;
  }

  /**
   * Get current row number.
   */
  uint64_t row_current()
  {
    return drizzle_row_current($self);
  }

  /* Skip all columns in result. */
  drizzle_return_t skip_column() {
    return drizzle_column_skip($self);
  }

  /**
   * Read column information.
   */
  Column *read_column(Column *column=NULL)
  {
    drizzle_return_t ret;
    return drizzle_column_read($self, column, &ret);
  }

  /**
   * Buffer all columns in result structure.
   */
  drizzle_return_t buffer_column()
  {
    return drizzle_column_buffer($self);
  }

  /* Use the following functions to use buffered columns. */
  Column *next_column() {
    return drizzle_column_next($self);
  }

  Column *prev_column() {
    return drizzle_column_prev($self);
  }

  void seek_column(uint16_t column) {
    drizzle_column_seek($self, column);
  }

  Column *column_index(uint16_t column) {
    return drizzle_column_index($self, column);
  }

  uint16_t current_column() {
    return drizzle_column_current($self);
  }

  %exception {
    $action
    if (check_drizzle_return(drizzle_return_value, drizzle_result_drizzle_con(arg1)) == false) {
      SWIG_fail;
    }
  }

  Result *buffer_result(drizzle_return_t *drizzle_return_value)
  {
    *drizzle_return_value= drizzle_result_buffer($self);
    return $self;
  }
  
  Field buffer_field(size_t *size, drizzle_return_t *drizzle_return_value)
  {
    return drizzle_field_buffer($self, size, drizzle_return_value);
  }
  
  /**
   * Read field for unbuffered result, possibly in parts. This is especially
  * useful for blob streaming, since the client does not need to buffer the
   * entire blob.
   */
  Field field_read(size_t *offset, size_t *size, size_t *total,
                   drizzle_return_t *drizzle_return_value)
  {
    return drizzle_field_read($self, offset, size, total, drizzle_return_value);
  }
  
  
  %noexception;

  /**
   * Get next row number for unbuffered results. Use the
   * drizzle_field* functions
   * to read individual fields after this function succeeds.
   */
  uint64_t read_row()
  {
    /* TODO: Actually check this */
    drizzle_return_t ret;
    uint64_t rownum= drizzle_row_read($self, &ret);
    if (ret != DRIZZLE_RETURN_OK)
      return 0;
    return rownum;
  }

  /**
   * Read and buffer one row.
   */
  drizzle_row_t buffer_row(row_buffer *new_buffer)
  {
    /* TODO: Actually check the return state */
    drizzle_return_t ret;
    
    new_buffer->row= drizzle_row_buffer($self, &ret);
    new_buffer->field_count= drizzle_result_column_count($self);
    new_buffer->result= $self;
    return (new_buffer->row);
  }

  /**
   * Free a row that was buffered with drizzle_row_buffer().
   */
  void free_row(drizzle_row_t row)
  {
    drizzle_row_free($self, row);
  }
  
  /**
   * Get current row number.
   */
  uint64_t current_row()
  {
    return drizzle_row_current($self);
  }


  void free_field(Field field)
  {
    drizzle_field_free(field);
  }

  /* Get information about a result. */
  bool eof()
  {
    return drizzle_result_eof($self);
  }

  unicode info()
  {
    return drizzle_result_info($self);
  }

  unicode error()
  {
    return drizzle_result_error($self);
  }

  uint16_t error_code()
  {
    return drizzle_result_error_code($self);
  }

  const char *sqlstate()
  {
    return drizzle_result_sqlstate($self);
  }

  uint16_t warning_count()
  {
    return drizzle_result_warning_count($self);
  }

  uint64_t insert_id()
  {
    return drizzle_result_insert_id($self);
  }

  uint64_t affected_rows()
  {
    return drizzle_result_affected_rows($self);
  }

  uint16_t column_count()
  {
    return drizzle_result_column_count($self);
  }

  uint64_t row_count()
  {
    return drizzle_result_row_count($self);
  }

  void set_eof(bool eof)
  {
    drizzle_result_set_eof($self, eof);
  }

  void set_info(const char *info)
  {
    drizzle_result_set_info($self, info);
  }

  void set_error(const char *error)
  {
    drizzle_result_set_error($self, error);
  }

  void set_error_code(uint16_t error_code)
  {
    drizzle_result_set_error_code($self, error_code);
  }

  void set_sqlstate(const char *sqlstate)
  {
    drizzle_result_set_sqlstate($self, sqlstate);
  }

  void set_warning_count(uint16_t warning_count)
  {
    drizzle_result_set_warning_count($self, warning_count);
  }

  void set_insert_id(uint64_t insert_id)
  {
    drizzle_result_set_insert_id($self, insert_id);
  }

  void set_affected_rows(uint64_t affected_rows)
  {
    drizzle_result_set_affected_rows($self, affected_rows);
  }

  void set_column_count(uint16_t column_count)
  {
    drizzle_result_set_column_count($self, column_count);
  }


  /* Initialize a column structure. */
  Column *create_column() {
    return drizzle_column_create($self, NULL);
  }

  drizzle_return_t write_column(Column *column)
  {
    return drizzle_column_write($self, column);
  }

  drizzle_return_t write_row()
  {
    return drizzle_row_write($self);
  }

}
