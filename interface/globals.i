/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(SWIGXML)
%include "typemaps.i"
#endif

%include "exception.i"
%include "stdint.i"

%feature("autodoc", "1");

%{
#include <libdrizzle/drizzle_client.h>
#include <libdrizzle/drizzle_server.h>

typedef struct drizzle_st Drizzle;
typedef struct drizzle_con_st Connection;
typedef struct drizzle_result_st Result;
typedef struct drizzle_column_st Column;
typedef struct drizzle_query_st Query;

typedef drizzle_field_t Field;
typedef drizzle_row_t Row;

typedef enum enum_drizzle_exception {
  DrizzleWarning,
  DrizzleError,
  InterfaceError,
  DatabaseError,
  DataError,
  OperationalError,
  IntegrityError,
  InternalError,
  ProgrammingError,
  NotSupportedError
} drizzle_exception;

typedef struct row_buffer_st
{
  drizzle_row_t row;
  uint16_t field_count;
  drizzle_result_st *result;
} row_buffer;

typedef const char* unicode;
%}

%ignore drizzle_state_fn;
%ignore drizzle_con_context_free_fn;
%ignore drizzle_query_context_free_fn;
%ignore drizzle_event_watch_fn;
%include <libdrizzle/constants.h>

%typedef uint32_t in_port_t;

%typedef drizzle_field_t field;
%typedef drizzle_row_t row;

%typemap(in, numinputs=0) (Field *field, size_t *size)
%{
  Field field$1;
  size_t size$2;

  $1= &field$1;
  $2= &size$2;
%}

%typemap(in, numinputs=0) row_buffer *result_buffer, row_buffer *new_buffer
%{
  row_buffer the_row$1;
  $1= &the_row$1;
%}

/* There should only ever be one of these per-function. Any more will have
   namespace collisions. But we specifically want to name it something
   known so we can reference it in exception handlers. */
%typemap(in, numinputs=0) (drizzle_return_t *drizzle_return_value)
%{
  drizzle_return_t drizzle_return_value;
  $1= &drizzle_return_value;
%}

