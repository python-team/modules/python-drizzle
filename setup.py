#!/usr/bin/env python
# -*- mode: python; c-basic-offset: 2; indent-tabs-mode: nil; -*-
#  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
#  drizzle-interface: Interface Wrappers for Drizzle
#  Copyright (c) 2009 Sun Microsystems
#  Copyright (c) 2010 Monty Taylor
#  All rights reserved.
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
# 
#  1. Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  2. Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#  3. The name of the author may not be used to endorse or promote products
#     derived from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
#  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import with_statement

NAME = "python-drizzle"
VERSION = "1.0"	

from distutils.core import setup, Command
from distutils.command.clean import clean
from distutils.command.build import build
from distutils.command.build_ext import build_ext
from distutils.command.sdist import sdist
from distutils.core import Extension
from distutils import util, file_util

import os.path, os
import sys
import subprocess

DESCRIPTION = """Python wrapper of libdrizzle"""

CLASSIFIERS="""\
Development Status :: 3 - Alpha
Intended Audience :: Developers
License :: OSI Approved :: BSD License
Operating System :: POSIX :: Linux
Operating System :: POSIX :: SunOS/Solaris
Operating System :: MacOS :: MacOS X
Programming Language :: C
Programming Language :: Python
Topic :: Database
Topic :: Software Development :: Libraries :: Python Modules
"""

class local_sdist(sdist):
    """Customized sdist hook - builds the changelog file from VC first"""

    def run(self):
        if os.path.isdir('.bzr'):
            # We're in a bzr branch

            log_cmd = subprocess.Popen(["bzr", "log", "--gnu"],
                                       stdout=subprocess.PIPE)
            changelog = log_cmd.communicate()[0]
            with open("changelog", "w") as changelog_file:
                changelog_file.write(changelog)
        sdist.run(self)

class local_build_ext(build_ext):
    """Customized build_ext hook - copy generated python into build dir"""
    def build_extension(self, ext):
        build_ext.build_extension(self, ext)
        build_py = self.get_finalized_command('build_py')
        package_dir = os.path.abspath(build_py.get_package_dir("drizzle"))
        build_dir = os.path.join(*([self.build_lib] + ["drizzle"]))
        if sys.version_info[0] == 2:
            file_util.copy_file(os.path.join(package_dir, "libdrizzle.py"), build_dir)
        elif sys.version_info[0] == 3:
            file_util.copy_file(os.path.join(package_dir, "libdrizzle.py"), build_dir)


SWIG_OPTS=['-O','-I.','-I/usr/include']
if sys.version_info[0] == 3:
  SWIG_OPTS.append("-py3")

setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=DESCRIPTION,
      author="Monty Taylor",
      author_email="mordred@inaugust.com",
      url="http://launchpad.net/drizzle-interface",
      platforms="linux",
      license="BSD",
      classifiers=filter(None, CLASSIFIERS.splitlines()),
      cmdclass={'sdist': local_sdist, 'build_ext': local_build_ext},
      package_dir={'': {2: 'lib', 3: 'lib3'}[sys.version_info[0]]},

      ext_modules=[
        Extension("drizzle._libdrizzle",
                  sources=[{'': {
                    2: "lib/drizzle/libdrizzle.i",
                    3: "lib3/drizzle/libdrizzle.i",
                  }[sys.version_info[0]]}['']],
                  swig_opts=SWIG_OPTS,
                  libraries=["drizzle"],
                  ),
        ],
      packages=["drizzle"],
      )


